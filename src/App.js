import {Fragment} from 'react'; // hindi magkaerror pag multiple components ang irereturn
import {Container} from 'react-bootstrap';
import {BrowserRouter as Router} from 'react-router-dom';
import { Routes, Route } from 'react-router-dom';
import AppNavbar from './components/AppNavbar';

import { useState } from 'react';
import { UserProvider } from './UserContext';


import Banner from './components/Banner';
import Highlights from './components/Highlights';

import Courses from './pages/Courses';
import Home from './pages/Home';
import Register from './pages/Register';
import Login from './pages/Login';
import Logout from './pages/Logout';
import Error from './pages/Error';

import './App.css';


// function App() {
//   return (
//     <Fragment>
//           <AppNavbar />
//         <Container>
//           <Banner />
//           <Highlights />
//         </Container>
//     </Fragment>
//   );
// }


function App() {

  const [user, setUser] = useState({
    email: localStorage.getItem('email')
  }); 

  const unsetUser = () => {
    localStorage.clear();
  }

  return (
    <UserProvider value = {{user, setUser, unsetUser}}>
      <Router>
          <AppNavbar/>
          <Container>
            <Routes>
              <Route exact path="/" element={<Home/>}></Route>
              <Route exact path="/courses" element={<Courses/>}></Route>
              <Route exact path="/login" element={<Login/>}></Route>
              <Route exact path="/logout" element={<Logout/>}></Route>
              <Route exact path="/register" element={<Register/>}></Route>
              <Route exact path="*" element={<Error/>}/>
            </Routes>
          </Container>
      </Router>
    </UserProvider>
  )
}

export default App;
